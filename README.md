m-epics-pvDataCPP conda recipe
==============================

Home: https://bitbucket.org/europeanspallationsource/m-epics-pvDataCPP

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: EPICS pvDataCPP module
